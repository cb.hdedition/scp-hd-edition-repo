using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashlightController : MonoBehaviour
{
    public static FlashlightController ins;


    private Quaternion currRotation, velRotation;
    [SerializeField]
    private float smallIntensity, bigIntensity, smallRadius, bigRadius, speed;

    [SerializeField]
    private Light light;

    [SerializeField]
    AudioClip flashClick;

    [SerializeField]
    private Vector3 addedRotation, flashOffset;

    private Quaternion addedQuat;

    public bool isSmall = false;
    public bool isOn = true;
    private GameItem currFlash;
    private Equipable_Elec_Loader flashDef;

    static bool playedTuto = false;


    // Start is called before the first frame update
    void Start()
    {
        ins = this;
        currFlash = GameController.ins.player.GetComponent<PlayerControl>().equipment[(int)bodyPart.Hand];
        flashDef = ((Equipable_Elec_Loader)ItemController.instance.items[currFlash.itemFileName]);
        currRotation = GameController.ins.currPly.PlayerCam.transform.rotation;
        isOn = true;
        if (!playedTuto)
        {
            playedTuto = true;
            SCP_UI.instance.ShowTutorial("tutoflashlight");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (GameController.ins.isAlive && GameController.ins.doGameplay)
        {
            if (flashDef.SpendBattery)
            {
                if (currFlash.valFloat > 0)
                {
                    addedQuat = Quaternion.Euler(addedRotation);
                    Vector3 planeRight = Vector3.ProjectOnPlane(GameController.ins.currPly.PlayerCam.transform.right, Vector3.up);
                    light.transform.position = GameController.ins.currPly.PlayerCam.transform.position + new Vector3(0f, flashOffset.y) + (planeRight * flashOffset.x);
                    currRotation = Lib.SmoothDamp(currRotation, GameController.ins.currPly.PlayerCam.transform.rotation, ref velRotation, speed);
                    light.transform.rotation = currRotation;

                    /* && !GameController.ins.currPly.Freeze*/

                    if (SCPInput.instance.playerInput.Gameplay.InteractNo.triggered || !isOn)
                    {
                        if (!isSmall)
                        {
                            light.intensity = smallIntensity;
                            light.spotAngle = smallRadius;
                            isSmall = true;
                        }
                        else
                        {
                            light.intensity = bigIntensity;
                            light.spotAngle = bigRadius;
                            isSmall = false;
                        }
                        GameController.ins.currPly.sfx.PlayOneShot(flashClick);
                        isOn=true;
                    }
                }
                else
                {
                    if (isOn)
                    {
                        light.intensity = 0;
                    }
                }
            }
        }   
    }
}
