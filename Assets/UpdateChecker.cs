using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using FullSerializer;
using UnityEngine.UI;
using Pixelplacement;

public class UpdateChecker : MonoBehaviour
{
    static bool alreadyChecked = false;
    [SerializeField]
    private Text header, body, accept, skip;
    [SerializeField]
    private GameObject holder, acceptButton;
    [SerializeField]
    private CanvasGroup panel;

    public struct UpdateData
    {
        [SerializeField]
        public string version;
    }
    void Start()
    {
        header.text=Localization.GetString("uiStrings", "ui_main_update");
        accept.text = Localization.GetString("uiStrings", "ui_up_update_download");
        skip.text = Localization.GetString("uiStrings", "ui_up_update_ok");
        if(!alreadyChecked)
            StartCoroutine(GetText());
    }

    public void FadePanel(float value)
    {
        panel.alpha= value;
    }

    IEnumerator GetText()
    {
        fsSerializer _serializer = new fsSerializer();
        yield return new WaitForSecondsRealtime(5f);

        UnityWebRequest www = UnityWebRequest.Get("https://raw.githubusercontent.com/ricky-daniel13/CB-HD-Edition-Update-Server/main/version.json");
        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.Log(www.error);
            ShowPanel(Localization.GetString("uiStrings", "ui_up_update_error"), Localization.GetString("uiStrings", "ui_up_update_download"), Localization.GetString("uiStrings", "ui_up_update_ok"), false);
        }
        else
        {
            // Show results as text
            //Debug.Log(www.downloadHandler.text);

            fsData data = fsJsonParser.Parse(www.downloadHandler.text);

            // step 2: deserialize the data
            UpdateData updData = new UpdateData();
            try
            {
                _serializer.TryDeserialize(data, ref updData).AssertSuccessWithoutWarnings();
                Debug.Log("Update: " + updData.version);
                if(updData.version != Application.version)
                {
                    string bodyText = Localization.GetString("uiStrings", "ui_up_update_message");
                    ShowPanel(string.Format(bodyText, Application.version, updData.version), Localization.GetString("uiStrings", "ui_up_update_download"), Localization.GetString("uiStrings", "ui_up_update_skip"), true);
                    alreadyChecked = true;
                }
            }
            catch (System.Exception)
            {
                ShowPanel(Localization.GetString("uiStrings", "ui_up_update_error"), Localization.GetString("uiStrings", "ui_up_update_download"), Localization.GetString("uiStrings", "ui_up_update_ok"), false);
            }
        }
    }

    void ShowPanel(string panelTxt, string acceptTxt, string cancelTxt, bool showAccept)
    {
        holder.SetActive(true);
        acceptButton.SetActive(showAccept);
        body.text = panelTxt;
        accept.text = acceptTxt;
        skip.text = cancelTxt;
        Tween.Value(0f, 1f, FadePanel, 0.5f, 0, Tween.EaseIn, Tween.LoopType.None);
    }

    public void DownloadNow()
    {
        Application.OpenURL("https://cbhdedition.itch.io/containment-breach-hd-edition");
    }

    public void HidePanel()
    {
        StartCoroutine(HidingPanel());
    }

    IEnumerator HidingPanel()
    {
        Tween.Value(1f, 0f, FadePanel, 0.25f, 0, Tween.EaseInStrong, Tween.LoopType.None, null);
        yield return new WaitForSecondsRealtime(0.25f);
        holder.SetActive(false);
    }
}
