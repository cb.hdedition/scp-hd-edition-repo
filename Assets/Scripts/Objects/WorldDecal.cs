using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldDecal : MonoBehaviour
{
    public int h, v;
    // Start is called before the first frame update
    void Awake()
    {
        DecalSystem.instance.DecalStatic(transform.position, transform.rotation, transform.localScale.x, h, v);
    }
}
