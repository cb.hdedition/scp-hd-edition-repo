using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.UI;
using Assets.Pixelation.Scripts;
using ThisOtherThing.UI.Shapes;

public class NightVisionController : MonoBehaviour
{
    class NpcDetected
    {
        public Vector3 position;
        public string name;
    }
    public Rectangle[] bats;
    [SerializeField]
    private Image batIcon;

    public static NightVisionController ins;

    [SerializeField]
    private GameObject detectorTemplate;

    [SerializeField]
    private GameObject batHolder;

    [SerializeField]
    private Canvas canvas;

    [SerializeField]
    private Text refresh;

    Equipable_NV NvDef;
    GameItem currNv;
    public PostProcessVolume vol;
    [System.NonSerialized]
    Pixelationv2 pix;
    bool closeTo895, efec895;
    [System.NonSerialized]
    public float distance;
    public RawImage jumpText;
    [SerializeField]
    float timeToUpdate, detectorRadius;
    float updateTimer;
    List<Text> displayList;
    List<NpcDetected> npcDataList;
    [SerializeField]
    LayerMask npcsMask;
    [System.NonSerialized]
    private NightVision nvPp;
    // Start is called before the first frame update
    void Start()
    {
        ins = this;
        currNv = GameController.ins.player.GetComponent<PlayerControl>().equipment[(int)bodyPart.Head];
        NvDef = ((Equipable_NV)ItemController.instance.items[currNv.itemFileName]);
        pix = vol.sharedProfile.GetSetting<Pixelationv2>();
        nvPp = vol.sharedProfile.GetSetting<NightVision>();
        nvPp.m_NVColor.Override(NvDef.nvColor);
        closeTo895 = false;
        distance = Mathf.Infinity;
        pix.BlockCount.Override(256f);
        displayList = new List<Text>();
        npcDataList = new List<NpcDetected>();

        if(!NvDef.SpendBattery)
            batHolder.SetActive(false);
        else
        {
            for (int i = 0; i < 10; i++)
            {
                bats[i].ShapeProperties.FillColor = NvDef.txtColor;
                bats[i].ShapeProperties.OutlineColor = NvDef.txtColor;
                bats[i].SetAllDirty();
            }
            batIcon.sprite = NvDef.batteryIcon;
        }

        refresh.color = NvDef.txtColor;

        if (!NvDef.isRadar)
        {
            refresh.gameObject.SetActive(false);
            
        }
        updateTimer = timeToUpdate;


    }

    // Update is called once per frame
    void Update()
    {
        if (GameController.ins.isAlive && GameController.ins.doGameplay)
        {
            if (NvDef.isRadar)
            {
                updateTimer -= Time.deltaTime;
                //Debug.Log("refreshTimer " + updateTimer);
                if (updateTimer < 0)
                {
                    GameController.ins.currPly.BlinkingTimer = 0;
                    GameController.ins.currPly.CloseTimer = GameController.ins.currPly.ClosedEyes;
                    updateTimer = timeToUpdate;
                    DetectorRefresh();
                }
                DetectorUpdate();
                refresh.text = "Refreshing data in\n" + updateTimer.ToString("00.0") + "\nSeconds";
            }

            //Debug.Log("bat" + currNv.valFloat);
            if (NvDef.SpendBattery)
            {
                if (currNv.valFloat > 0)
                {
                    int batPercent = (Mathf.CeilToInt((10 * currNv.valFloat) / 100));

                    for (int i = 0; i < 10; i++)
                    {
                        if (batPercent > 0)
                            bats[i].ShapeProperties.DrawFill = true;
                        else
                            bats[i].ShapeProperties.DrawFill = false;
                        bats[i].SetAllDirty();
                        batPercent -= 1;
                    }
                }
                else
                {
                    GameController.ins.currPly.BlinkingTimer = 0;
                    GameController.ins.currPly.CloseTimer = GameController.ins.currPly.ClosedEyes;
                }
            }

            if (Time.frameCount % 15 == 0)
            {
                Vector2Int roomPos = GameController.ins.GetRoom("Heavy_COFFIN");
                if (roomPos.x == -1)
                {
                    closeTo895 = false;
                }
                else
                {
                    Vector2Int playerPos = new Vector2Int(GameController.ins.xPlayer, GameController.ins.yPlayer);
                    distance = Vector2.Distance(playerPos, roomPos);
                    //Debug.Log("Distance: " + distance);
                    if (distance < 2f)
                    {
                        closeTo895 = true;
                    }
                    else
                    {
                        closeTo895 = false;
                    }
                }
            }

            if (closeTo895 && (currNv.valFloat > 0))
            {
                efec895 = true;
                float flick = Lib.EvalWave(WaveFunctions.Noise, 0, Mathf.Lerp(0.125f, 0.5f, distance / 2f), 1);
                if (flick > 0.5f)
                {
                    pix.BlockCount.Override((distance > 0.9f) ? 64f : 32f);
                }
                else
                {
                    pix.BlockCount.Override((distance > 0.9f) ? 256 : 128);
                }
            }
            else if (efec895)
            {
                pix.BlockCount.Override(256f);
                efec895 = false;
            }
        }
    }

    void DetectorRefresh()
    {
        Collider[] npcs = Physics.OverlapSphere(GameController.ins.currPly.transform.position, detectorRadius, npcsMask, QueryTriggerInteraction.Ignore);
        for(int i = 0; i < npcs.Length; i++)
        {
            if(displayList.Count < i+1)
            {
                displayList.Add(Instantiate(detectorTemplate, this.gameObject.transform).GetComponent<Text>());
                npcDataList.Add(new NpcDetected());
            }

            string npcName = "Human";
            Map_NPC npc = npcs[i].GetComponent<Map_NPC>();
            if (npc != null)
                npcName = npc.charName;

            npcDataList[i].name = npcName;
            npcDataList[i].position = (npcs[i].transform.position + new Vector3(0f, 1.75f, 0f));
            Debug.DrawLine(npcs[i].transform.position, npcs[i].transform.position + new Vector3(0f, 1.75f, 0f), Color.blue, timeToUpdate);
        }

        if(npcs.Length > displayList.Count)
        {
            for(int i = npcs.Length; i < displayList.Count; i++)
            {
                Destroy(displayList[i].gameObject);
            }
            displayList.RemoveRange(npcs.Length, displayList.Count- npcs.Length);
            npcDataList.RemoveRange(npcs.Length, npcDataList.Count - npcs.Length);
        }
    }

    void DetectorUpdate()
    {
        for(int i = 0; i < npcDataList.Count; i++)
        {
            float distance = Vector3.Distance(npcDataList[i].position, GameController.ins.currPly.transform.position);

            displayList[i].text = npcDataList[i].name + "\n" + distance.ToString("000") + "m";
            displayList[i].color = NvDef.txtColor;

            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.GetComponent<RectTransform>(), Lib.CameraToScreenDisp(GameController.ins.currPly.PlayerCam, npcDataList[i].position), null, out Vector2 newPosition))
            {
                displayList[i].rectTransform.anchoredPosition = newPosition;
            };
        }
    }

}
