using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NightVision", menuName = "Items/Night Vision")]
public class Equipable_NV : Equipable_Elec_Loader
{
    public bool isRadar;
    public Color nvColor;
    public Color txtColor;
    public Sprite batteryIcon;
    public override void OnEquip(ref GameItem currItem)
    {
        base.OnEquip(ref currItem);
    }

    public override void OnDequip(ref GameItem currItem)
    {
        base.OnDequip(ref currItem);
    }
}
