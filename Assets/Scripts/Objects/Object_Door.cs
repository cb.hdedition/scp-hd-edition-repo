﻿using Unity.AI.Navigation;
using UnityEngine;
using UnityEngine.AI;

public class Object_Door : MonoBehaviour
{
    public static int openDoorFlag=0, closedDoorFlag = 8;
    [SerializeField]
    private GameObject Door01, Door02;
    [SerializeField]
    private float OpenSpeed, DoorEndPos, DoorTime;
    private int id;

    [SerializeField]
    private AudioClip[] Open_AUD;
    [SerializeField]
    private AudioClip[] SCP_AUD;
    [SerializeField]
    private AudioClip[] Close_AUD;
    [SerializeField]
    private AudioSource AUD;

    [SerializeField]
    private NavMeshObstacle carveMesh;
    [SerializeField]
    private NavMeshLink doorLink;
    [SerializeField]
    private bool UseParticle = false;

    Vector3 Pos1, Pos2;
    float moveAmmount = 0;
    public bool isOpen = false, scp173 = true, ignoreSave = false, isDisabled = false;
    public bool currentlyOpen = false;
    public bool isMoving = false;
    bool isForcing = false;
    bool lastStateOpen;

    // Start is called before the first frame update

    private void Awake()
    {
        openDoorFlag = NavMesh.GetAreaFromName("Walkable");
        closedDoorFlag = NavMesh.GetAreaFromName("ClosedDoor");
        AUD = GetComponent<AudioSource>();
        Pos1 = Door01.transform.position;
        Pos2 = Door02.transform.position;
    }

    void Start()
    {
        if (!ignoreSave)
        {
            id = GameController.ins.GetDoorID();
            transform.parent = GameController.ins.doorParent.transform;
            ResetState();
        }
    }

    public void ResetState()
    {
        int doorState = GameController.ins.GetDoorState(id);
        if (doorState != -1)
        {
            if (doorState == 0)
            {
                currentlyOpen = true;
                isOpen = false;
                moveAmmount = DoorEndPos;
            }
            if (doorState == 1)
            {
                currentlyOpen = false;
                isOpen = true;
                moveAmmount = 0;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (currentlyOpen == false && isOpen == true)
            DoorOpen();

        if (isOpen == false && currentlyOpen == true)
            DoorClose();

        if (isForcing == true)
            Hodor();
    }

    void DoorOpen()
    {
        if (moveAmmount < DoorEndPos)
        {
            moveAmmount += OpenSpeed * Time.deltaTime;
            if (moveAmmount > DoorEndPos)
                moveAmmount = DoorEndPos;

            Door01.transform.position = Pos1 - (Door01.transform.right * moveAmmount);
            Door02.transform.position = Pos2 - (Door02.transform.right * moveAmmount);
        }
        else
        {
            currentlyOpen = true;
            doorLink.area = openDoorFlag;
            if (!ignoreSave)
                GameController.ins.SetDoorState(true, id);
        }


    }

    void DoorClose()
    {
        if (moveAmmount > 0)
        {
            moveAmmount -= OpenSpeed * Time.deltaTime;
            if (moveAmmount < 0)
                moveAmmount = 0;

            Door01.transform.position = Pos1 - (Door01.transform.right * moveAmmount);
            Door02.transform.position = Pos2 - (Door02.transform.right * moveAmmount); 
        }
        else
        {
            if (UseParticle)
            {
                GameController.ins.particleController.StartParticle(1, transform.position, transform.rotation);
            }
            doorLink.area = closedDoorFlag;
            currentlyOpen = false;
            if (!ignoreSave)
                GameController.ins.SetDoorState(false, id);
        }
    }

    public void DoorSwitch()
    {

        if (!isDisabled)
        {
            if (isOpen == true && currentlyOpen == true)
            {
                isOpen = false;
                PlayClose();
                moveAmmount = DoorEndPos;
            }
            if (isOpen == false && currentlyOpen == false)
            {
                isOpen = true;
                PlayOpen();
                moveAmmount = 0;
            }
        }
    }

    public void DoorSwitch(bool val)
    {
        if (isOpen != val)
        {
            currentlyOpen = !val;
            if (val)
                PlayOpen();
            else
                PlayClose();
        }
        isOpen = val;
    }

    public void InstantSet(bool open)
    {
        if (!isDisabled)
        {
            if (open == false)
            {
                isOpen = false;
                currentlyOpen = false;
                Door01.transform.position = Pos1;
                Door02.transform.position = Pos2;
                doorLink.area = closedDoorFlag;
                carveMesh.enabled = false;
                if (!ignoreSave)
                    GameController.ins.SetDoorState(false, id);
            }
            else
            {
                currentlyOpen = true;
                isOpen = true;
                Door01.transform.position = Pos1 - (Door01.transform.right * DoorEndPos);
                Door02.transform.position = Pos2 - (Door02.transform.right * DoorEndPos);
                doorLink.area = openDoorFlag;
                carveMesh.enabled = true;
                if (!ignoreSave)
                    GameController.ins.SetDoorState(true, id);
            }
        }
    }

    public void ForceOpen(float time)
    {
        if (!isDisabled)
        {
            if (!isForcing)
            {
                //Debug.Log("ForceDoor start!, door was " + isOpen);
                lastStateOpen = isOpen;
            }
            isForcing = true;
            DoorTime = time;
        }
    }

    void Hodor()
    {
        if (isOpen != true)
            DoorSwitch();
        DoorTime -= (Time.deltaTime);
        if (DoorTime <= 0.0f)
        {
            isForcing = false;
            if(lastStateOpen!=isOpen)
                DoorSwitch();

            //Debug.Log("ForceDoor end!");
        }
    }



    public bool Door173()
    {
        if (isOpen == false && currentlyOpen == false && scp173 == true && !isDisabled)
        {
            isOpen = true;
            PlaySCP(0);
            moveAmmount = 0;
            return (true);
        }
        else
        {
            return (false);
        }
    }

    void PlayOpen()
    {
        AUD.clip = Open_AUD[Random.Range(0, Open_AUD.Length)];
        AUD.Play();
    }
    void PlaySCP(int i)
    {
        AUD.clip = SCP_AUD[i];
        AUD.Play();
    }
    void PlayClose()
    {
        AUD.clip = Close_AUD[Random.Range(0, Close_AUD.Length)];
        AUD.Play();
    }
    /// <summary>
    /// GetState() Obtiene el estado de la puerta, True si Abierta
    /// </summary>
    /// <returns>true Si la puerta esta abierta</returns>
    public bool GetState()
    {
        return (currentlyOpen&&isOpen);
    }

}
