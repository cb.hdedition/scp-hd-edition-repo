﻿/// <summary>
/// Marshals events and data between ConsoleController and uGUI.
/// Copyright (c) 2014-2015 Eliot Lash
/// </summary>
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using System.Collections;

public class ConsoleView : MonoBehaviour
{
    ConsoleController console = new ConsoleController();

    bool didShow = false;

    public Canvas viewContainer; //Container for console view, should be a child of this GameObject
    public Text logTextArea;
    public InputField inputField;

    void Start()
    {
        viewContainer.enabled = false;
        if (console != null)
        {
            console.visibilityChanged += onVisibilityChanged;
            console.logChanged += onLogChanged;
            Application.logMessageReceived += console.Callback;
        }
        
        updateLogStr(console.log);
        
    }

    ~ConsoleView()
    {
        console.visibilityChanged -= onVisibilityChanged;
        console.logChanged -= onLogChanged;
    }

    private void OnDestroy()
    {
        Application.logMessageReceived -= console.Callback;
    }

    void Update()
    {
        //Toggle visibility when tilde key pressed
        if (SCPInput.instance.playerInput.Gameplay.Debug.triggered)
        {
            toggleVisibility(SCP_UI.instance.ToggleConsole());
        }
    }

    void toggleVisibility(bool value)
    {
        setVisibility(value);
    }

    void setVisibility(bool visible)
    {
        viewContainer.enabled = visible;
    }

    void onVisibilityChanged(bool visible)
    {
        setVisibility(visible);
    }

    void onLogChanged(string[] newLog)
    {
        updateLogStr(newLog);
    }

    void updateLogStr(string[] newLog)
    {
        if (newLog == null)
        {
            logTextArea.text = "";
        }
        else
        {
            logTextArea.text = string.Join("\n", newLog);
        }
    }

    /// <summary>
    /// Event that should be called by anything wanting to submit the current input to the console.
    /// </summary>
    public void runCommand()
    {
        console.runCommandString(inputField.text);
        inputField.text = "";
    }

}