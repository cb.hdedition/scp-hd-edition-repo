using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class AmbientLightController : MonoBehaviour
{
    public float AmbientLightContribution;
    // Start is called before the first frame update
    void Awake()
    {
        UpdateAmbient();
    }

    private void OnValidate()
    {
        UpdateAmbient();
    }

    void UpdateAmbient()
    {
        Shader.SetGlobalFloat("ProbeDifContribution", AmbientLightContribution);
    }



}
