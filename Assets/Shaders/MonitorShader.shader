Shader "Custom/MonitorShader"
{
    Properties
    {
        //_ScreenColor ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo+Smoothness (RGBA)", 2D) = "white" {}
        _ScreenTex("Screen (RGB)", 2D) = "white" {}
        _StaticTex("Static (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Intensity("Intensity", Float) = 1
    }
    CGINCLUDE
    #include "Assets/Shaders/Library/ShaderLib.cginc"
    ENDCG
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard nofog

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;
        sampler2D _ScreenTex;
        sampler2D _StaticTex;

        struct Input
        {
            float2 uv_MainTex;
            float2 uv_ScreenTex;
            float2 uv_StaticTex;
        };

        half _Glossiness;
        half _Intensity;
        //fixed4 _Color;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
            o.Albedo = c.rgb;
            o.Metallic = 0;
            o.Smoothness = c.a*_Glossiness;

            float4 cam=tex2D(_ScreenTex, IN.uv_ScreenTex);
            float4 over=tex2D(_StaticTex, IN.uv_StaticTex);
            cam *= _Intensity;
            cam = ScreenBlend(cam, over);
            o.Emission = cam;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
