// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Custom/DitherTransparency"
{
	Properties
	{
		_Cutoff( "Mask Clip Value", Float ) = 1
		_MainTex("albedoMap", 2D) = "white" {}
		[NoScaleOffset][Normal]_normalMap("normalMap", 2D) = "bump" {}
		_AlphaMult("AlphaMult", Range( 0 , 1)) = 1
		[NoScaleOffset]_metallicMap("metallicMap", 2D) = "black" {}
		_normalPower("normalPower", Float) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "Geometry+0" }
		Cull Off
		//ZWrite Off
		CGPROGRAM
		#include "UnityStandardUtils.cginc"
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows exclude_path:forward nolightmap  nodynlightmap nodirlightmap nofog vertex:vertexDataFunc 
		struct Input
		{
			float2 uv_MainTex;
			float2 uv_texcoord;
			float4 screenPosition;
		};

		uniform sampler2D _normalMap;
		uniform half _normalPower;
		uniform half4 _normalMap_ST;
		uniform sampler2D _MainTex;
		uniform sampler2D _metallicMap;
		uniform half _AlphaMult;
		uniform float _Cutoff = 1;


		inline float Dither4x4Bayer( int x, int y )
		{
			const float dither[ 16 ] = {
				 1,  9,  3, 11,
				13,  5, 15,  7,
				 4, 12,  2, 10,
				16,  8, 14,  6 };
			int r = y * 4 + x;
			return dither[r] / 16; // same # of instructions as pre-dividing due to compiler magic
		}


		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float4 ase_screenPos = ComputeScreenPos( UnityObjectToClipPos( v.vertex ) );
			o.screenPosition = ase_screenPos;
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_normalMap = i.uv_MainTex;
			o.Normal = UnpackScaleNormal( tex2D( _normalMap, uv_normalMap ), _normalPower );
			float2 uv_MainTex4 = i.uv_MainTex;
			half4 tex2DNode4 = tex2D( _MainTex, uv_MainTex4 );
			o.Albedo = tex2DNode4.rgb;
			float2 uv_metallicMap12 = i.uv_MainTex;
			half4 tex2DNode12 = tex2D( _metallicMap, uv_metallicMap12 );
			o.Metallic = tex2DNode12.r;
			o.Smoothness = tex2DNode12.a;
			o.Alpha = 1;
			float4 ase_screenPos = i.screenPosition;
			half4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			half2 clipScreen7 = ase_screenPosNorm.xy * _ScreenParams.xy;
			half dither7 = Dither4x4Bayer( fmod(clipScreen7.x, 4), fmod(clipScreen7.y, 4) );
			dither7 = step( dither7, ( tex2DNode4.a * _AlphaMult ) );
			clip( dither7 - _Cutoff );
		}

		ENDCG
	}
	Fallback "Diffuse"
}
/*ASEBEGIN
Version=17900
1360;-256;1280;963;1313.498;504.2072;1.3;True;True
Node;AmplifyShaderEditor.TexturePropertyNode;1;-819.3599,-449.1016;Inherit;True;Property;_MainTex;albedoMap;1;1;[NoScaleOffset];Create;True;0;0;False;0;196de0b02fc2658449cab405cdd519be;196de0b02fc2658449cab405cdd519be;False;white;Auto;Texture2D;-1;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.SamplerNode;4;-552.4744,-434.3638;Inherit;True;Property;_TextureSample0;Texture Sample 0;3;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;9;-567.3848,-201.4448;Inherit;False;Property;_AlphaMult;AlphaMult;3;0;Create;True;0;0;False;0;1;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;13;-1053.498,161.3924;Inherit;False;Property;_normalPower;normalPower;5;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;10;-260.7909,-332.3166;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TexturePropertyNode;11;-1097.991,-229.6169;Inherit;True;Property;_metallicMap;metallicMap;4;1;[NoScaleOffset];Create;True;0;0;False;0;None;None;False;black;Auto;Texture2D;-1;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.TexturePropertyNode;2;-1006.531,274.9215;Inherit;True;Property;_normalMap;normalMap;2;1;[Normal];Create;True;0;0;False;0;None;None;False;bump;Auto;Texture2D;-1;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.SamplerNode;8;-704.1539,276.8199;Inherit;True;Property;_TextureSample1;Texture Sample 1;3;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DitheringNode;7;-93.57422,-187.3633;Inherit;False;0;False;3;0;FLOAT;0;False;1;SAMPLER2D;;False;2;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;12;-601.3907,-65.81665;Inherit;True;Property;_TextureSample2;Texture Sample 2;5;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;106.6,-209.3;Half;False;True;-1;2;;0;0;Standard;ditherTransparency;False;False;False;False;False;False;True;True;True;True;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;1;True;True;0;True;TransparentCutout;;Geometry;DeferredOnly;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;4;0;1;0
WireConnection;10;0;4;4
WireConnection;10;1;9;0
WireConnection;8;0;2;0
WireConnection;8;5;13;0
WireConnection;7;0;10;0
WireConnection;12;0;11;0
WireConnection;0;0;4;0
WireConnection;0;1;8;0
WireConnection;0;3;12;1
WireConnection;0;4;12;4
WireConnection;0;10;7;0
ASEEND*/
//CHKSM=80AC732A1C9621300B53BFCB8A08361EE437AD85