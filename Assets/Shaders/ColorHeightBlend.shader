Shader "Custom/ColorHeightBlend"
{
    Properties
    {
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _HeightMap ("HeightMap", 2D) = "white" {}
        _Metalness ("Metallic + smoothness", 2D) = "white" {}
        _BumpMap ("Normal Map", 2D) = "bump" {}
        _Occlusion ("Oclusion Map", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 1
        _BumpPower ("NormalPower", Float) = 1
        _MainTex2 ("Albedo (RGB)2", 2D) = "white" {}
        _HeightMap2 ("HeightMap2", 2D) = "white" {}
        _Metalness2 ("Metallic + smoothness2", 2D) = "white" {}
        _BumpMap2 ("Normal Map2", 2D) = "bump" {}
        _Occlusion2 ("Oclusion Map2", 2D) = "white" {}
        _Glossiness2 ("Smoothness2", Range(0,1)) = 1
        _BumpPower2 ("NormalPower2", Float) = 1
        _HeightblendFactor("Heightmap Blending", Float) = 0.05
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;
        sampler2D _HeightMap;
        sampler2D _Metalness;
        sampler2D _BumpMap;
        sampler2D _Occlusion;
        sampler2D _MainTex2;
        sampler2D _HeightMap2;
        sampler2D _Metalness2;
        sampler2D _BumpMap2;
        sampler2D _Occlusion2;

        struct Input
        {
            float2 uv_MainTex;
            float2 uv_MainTex2;
            float4 color : COLOR;
        };

        half _Glossiness;
        half _BumpPower;
        half _Glossiness2;
        half _BumpPower2;
        half _HeightblendFactor;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        float heightblendScalar(float input1, float height1, float input2, float height2)
        {  
            float height_start = max(height1, height2) - _HeightblendFactor;
            float level1 = max(height1 - height_start, 0);
            float level2 = max(height2 - height_start, 0);
            return ((input1 * level1) + (input2 * level2)) / (level1 + level2);
        }
        
        float heightlerpScalar(float input1, float height1, float input2, float height2, float t)
        {
            t = clamp(t, 0, 1);
            return heightblendScalar(input1, height1 * (1 - t), input2, height2 * t);
        }

        float4 heightblend(float4 input1, float4 height1, float4 input2, float4 height2)
        {  
            float height_start = max(height1, height2) - _HeightblendFactor;
            float level1 = max(height1 - height_start, 0);
            float level2 = max(height2 - height_start, 0);
            return ((input1 * level1) + (input2 * level2)) / (level1 + level2);
        }
        
        float4 heightlerp(float4 input1, float4 height1, float4 input2, float4 height2, float t)
        {
            t = clamp(t, 0, 1);
            return heightblend(input1, height1 * (1 - t), input2, height2 * t);
        }

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            fixed4 c1 = tex2D (_MainTex, IN.uv_MainTex);
            fixed4 c2 = tex2D (_MainTex2, IN.uv_MainTex2);

            fixed4 h1 = tex2D (_HeightMap, IN.uv_MainTex);
            fixed4 h2 = tex2D (_HeightMap, IN.uv_MainTex2);

            float splat = IN.color.r;

            o.Albedo = heightlerp(c1, h1, c2, h2, splat);

            fixed4 metsmo = tex2D (_Metalness, IN.uv_MainTex);
            fixed4 metsmo2 = tex2D (_Metalness, IN.uv_MainTex);

            fixed4 blendSpec = heightlerp(metsmo, h1, metsmo2, h2, splat);
            half blendSpecScalar = heightlerpScalar(_Glossiness, h1, _Glossiness2, h2, splat);

            o.Metallic = blendSpec.r;
            o.Smoothness = blendSpec.a * blendSpecScalar;

            
            fixed4 occ = tex2D (_Occlusion, IN.uv_MainTex);
            fixed4 occ2 = tex2D (_Occlusion2, IN.uv_MainTex);
            
            half blendOccScalar = heightlerpScalar(occ, h1, occ2, h2, splat);

            o.Occlusion = occ.r;

            fixed4 normal = tex2D(_BumpMap, IN.uv_MainTex);
            fixed4 normal2 = tex2D(_BumpMap2, IN.uv_MainTex);

            half blendNormalScalar = heightlerpScalar(_BumpPower, h1, _BumpPower2, h2, splat);

            fixed4 blendNormal = heightlerp(normal, h1, normal2, h2, splat);

            o.Normal = UnpackScaleNormal(blendNormal, blendNormalScalar);

            o.Alpha = 1;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
