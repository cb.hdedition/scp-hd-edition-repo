#ifndef SHADER_LIB_INCLUDED
#define SHADER_LIB_INCLUDED

float4 ScreenBlend(float4 s, float4 d)
{
    return s + d - s * d;
}

#endif