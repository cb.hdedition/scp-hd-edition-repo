using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TouchableLink : MonoBehaviour, IPointerClickHandler
{
    // Start is called before the first frame update
    [SerializeField]
    string url;

    public void OnPointerClick(PointerEventData eventData)
    {
        Application.OpenURL(url);
    }
}
