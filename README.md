**Containment Breach HD Edition - Source Code**

This game and its source code is licensed under Creative Commons Attribution-ShareAlike 3.0 License.
http://creativecommons.org/licenses/by-sa/3.0/

This is a reupload of the original source code that was hosted on Github. Most of the commit story before 0.3.0 is lost.

The code is under version 2020.3.48f1 of Unity, developed using a free license. It uses the AI Package, the new Input System package, and the Post Procesing V2 package. It also uses custom solutions for Inverse square lighting decay, decal rendering, and ambient lighting.

 The code is heavily dependent on Unity script execution order. This should be changed in the future for a more reliable solution. Too many components are tightly coupled and many choices are done with a missinformed understanding on how C# and Unity works. This are all issues that should be solved in the future.

